import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;


/**
 * Exemplo de impress�o de um objeto Printable
 * 
 * @author Herval Freire
 */
public class PrintableExample {

	public PrintableExample() {
		// Obtem um job de impressao
		PrinterJob job = PrinterJob.getPrinterJob();
		
		// Define o objeto a ser impresso
		job.setPrintable(new Desenho());
		
		
		
		// exibe o dialogo de impressao.
		if (job.printDialog()) {
			try {
				// imprime o objeto printable
				job.print();
			} catch (PrinterException e) {
				e.printStackTrace();
			}
		}	
		
	}
	
	public static void main(String[] args) {
		new PrintableExample();
	}
}