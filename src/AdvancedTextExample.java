import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * Exemplos de manipula��o avan�ada de textos para
 * impress�o
 * 
 * @author Herval Freire
 */
public class AdvancedTextExample implements Printable {
	
	// array de paragrafos do texto
	ArrayList texto;
	
	public AdvancedTextExample() throws IOException {
		// Obtem um job de impressao
		PrinterJob job = PrinterJob.getPrinterJob();
		
		// carrega o texto a ser impresso
		BufferedReader re = new BufferedReader(new FileReader("texto.txt"));
		
		texto = new ArrayList();
		String line = re.readLine();
		while (line != null) {
			texto.add(line);
			line = re.readLine();
		}		
		re.close();
		
		
		// Define o objeto a ser impresso
		job.setPrintable(this);
		
		// exibe o dialogo de impressao.
		if (job.printDialog()) {
			try {
				// imprime o objeto printable
				job.print();
			} catch (PrinterException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public int print(Graphics g, PageFormat format, int page) throws PrinterException {
		if (page != 0) {
			return NO_SUCH_PAGE;
		}
		Graphics2D gr = (Graphics2D) g;

		// determina o ponto de inicio do texto (inicio da area util + 10 pontos)
		float posX = (float) format.getImageableX()+10;
		float posY = (float) format.getImageableY()+10;

		// determina a largura do texto como 350 pontos (dpi)
		float larguraTexto = 350;
		

		// para cada paragrafo, imprime o paragrafo formatado
		Iterator it = texto.iterator();
		while (it.hasNext()) {
			String line = (String) it.next();
		
			// caso haja uma linha em branco, substituir por um espa�o
			// para permitir formata��o
			if (line.length() == 0) {
				line = " ";
			}
			
			// texto formatado a ser impresso
			AttributedString str = new AttributedString(line);
		
			// define a fonte do texto como arial 12 it�lico
			str.addAttribute(TextAttribute.FONT, new Font("Arial", Font.ITALIC, 12));

			// instancia um line breaker para o texto formatado
			LineBreakMeasurer quebrador = new LineBreakMeasurer(str.getIterator(), 
					gr.getFontRenderContext());		
		
			// cria um TextLayout para armazenar cada linha 'quebrada'
			TextLayout linha = quebrador.nextLayout(larguraTexto);
			while (linha != null) {

				// posiciona o texto
				posY += linha.getAscent();
				
				linha.draw(gr, posX, posY);
				
				// soma espa�o para a proxima linha
				posY += linha.getDescent() + linha.getLeading();
				
				linha = quebrador.nextLayout(larguraTexto);
			}
			
		} // fim dos paragrafos
		
		return PAGE_EXISTS;
	}

	
	public static void main(String[] args) {
		try {
			new AdvancedTextExample();
		} catch (IOException e) {
			System.out.println("Erro imprimindo: " + e.getMessage());
		}
	}
}