import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;


/**
 * Exemplos de manipula��o de imagens para impressao
 * 
 * @author Herval Freire
 */
public class ImagePrintExample implements Printable {
	
	String imagem = "severina.jpg";
	
	public ImagePrintExample() {
		// Obtem um job de impressao
		PrinterJob job = PrinterJob.getPrinterJob();
		
		// Define o objeto a ser impresso
		job.setPrintable(this);
		
		// exibe o dialogo de impressao.
		if (job.printDialog()) {
			try {
				// imprime o objeto printable
				job.print();
			} catch (PrinterException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public int print(Graphics g, PageFormat format, int page) throws PrinterException {
		if (page != 0) {
			return NO_SUCH_PAGE;
		}
		Graphics2D gr = (Graphics2D) g;
		
		// posiciona o objeto graphics no come�o da area util da pagina
		gr.translate(format.getImageableX(), format.getImageableY());

		
		// carrega a imagem do arquivo jpg
		Image image = Toolkit.getDefaultToolkit().getImage(imagem);
	    MediaTracker mediaTracker = new MediaTracker(new Container());
	    mediaTracker.addImage(image, 0);
	    try {
			mediaTracker.waitForID(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
				
		// imprime a imagem na posicao relativa 10, 10
		gr.drawImage(image, 10, 10, null);
		 
		return PAGE_EXISTS;
	}

	
	public static void main(String[] args) {
		new ImagePrintExample();
	}


}
