import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;


/**
 * Exemplo de impress�o de um objeto Pageable
 * 
 * @author Herval Freire
 */
public class PageableExample {

	public PageableExample() {
		// Obtem um job de impressao
		PrinterJob job = PrinterJob.getPrinterJob();
		
		// Define o objeto a ser impresso
		job.setPageable(new Livro());
		
		// exibe o dialogo de impressao.
		if (job.printDialog()) {
			try {
				// imprime o objeto printable
				job.print();
			} catch (PrinterException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public static void main(String[] args) {
		new PageableExample();		
	}
}

// defini��o de uma classe 'pageable' (um Book com 3 p�ginas)
class Livro extends Book {

	public Livro() {
		PageFormat formatoCapa = new PageFormat();
		formatoCapa.setOrientation(PageFormat.PORTRAIT);
		
		PageFormat formatoPagina = new PageFormat();
		formatoPagina.setOrientation(PageFormat.LANDSCAPE);
		// define o tamanho da pagina em 8,5x11 polegadas (Letter)
		formatoPagina.getPaper().setSize(612, 792);
		
		// Adiciona uma capa ao livro utilizando a formata��o de capa
		append(new Capa(), formatoCapa);
		
		// Adiciona duas paginas ao livro utilizando a formatacao de pagina interna
		append(new Pagina(), formatoPagina);
		append(new Pagina(), formatoPagina);
		
		System.out.println("Livro criado. Total de paginas: " + getNumberOfPages());
	}
	
}


// Renderiza a capa: um retangulo ao redor da area util da pagina
class Capa implements Printable {

	public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {
		if (pageIndex > 1) {
			return Printable.NO_SUCH_PAGE;
		}
		
		g.drawRect((int) pageFormat.getImageableX()+1, (int) pageFormat.getImageableY()+1, 
				(int) pageFormat.getImageableWidth()-2, (int) pageFormat.getImageableHeight()-2);
		
		return Printable.PAGE_EXISTS;
	}
	
}

// Renderiza uma pagina do livro: apenas um texto impresso no meio da pagina
class Pagina implements Printable {

	public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {
		// Imprime somente os indices 0 e 1
		if (pageIndex > 2) {
			return Printable.NO_SUCH_PAGE;
		}
		g.drawString("JavaMagazine", 
				(int) pageFormat.getImageableWidth()/3, 
				(int) pageFormat.getImageableHeight()/2);
		
		return Printable.PAGE_EXISTS;
	}
	
}