import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

//defini��o de uma classe 'printable'
public class Desenho implements Printable {

	// M�todo de renderiza��o de paginas
	public int print(Graphics graphics, PageFormat format, int pageIndex) throws PrinterException {
		// (caso o pageIndex seja maior que zero, retorna NO_SUCH_PAGE)
		if (pageIndex > 0) {
	        return Printable.NO_SUCH_PAGE;
	    }
		
		Graphics2D g2d = (Graphics2D) graphics;
		
		// desenha um retangulo na pagina
	    int x = 85;
	    int y = 87;
		g2d.draw(new Rectangle2D.Double(x, y, 400, 300));
		
		
		// retorna o indice da pagina atual (zero)
		return Printable.PAGE_EXISTS;
	}
}