import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.SimpleDoc;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;

/**
 * Exemplo de impress�o utilizando a JPS
 * 
 * @author Herval Freire
 */
public class JpsPrintExample {
	public JpsPrintExample(boolean showDialog) {
		try {
			// Localiza todas as impressoras com suporte a arquivos txt
			PrintService[] printerServices = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.AUTOSENSE, null);
			System.out.println("Impressoras com suporte: " + printerServices.length);
			
			// Localiza a impressora padr�o
			PrintService printer = PrintServiceLookup.lookupDefaultPrintService();
			System.out.println("Impressora: " + printer.getName());
			
			System.out.println("Imprimindo arquivo txt");
			// Defini��o de atributos do conte�do a ser impresso:
			DocFlavor docFlavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
	
			// Atributos de impress�o do documento
			HashDocAttributeSet attributes = new HashDocAttributeSet();
			
			// InputStream apontando para o conte�do a ser impresso
			FileInputStream fi = new FileInputStream("teste.txt");
			
			// Cria um Doc para impress�o a partir do arquivo exemplo.txt
			Doc textDocument = new SimpleDoc(fi, docFlavor, attributes);
			

			// Configura o conjunto de parametros para a impressora
			PrintRequestAttributeSet printerAttributes = new HashPrintRequestAttributeSet();
			
			if (showDialog) {
				// exibe um dialogo de configuracoes de impressao
				PrintService service =  ServiceUI.printDialog(null, 320, 240,
					    printerServices, printer, docFlavor, printerAttributes);

				if (service != null) {
					DocPrintJob printJob = service.createPrintJob();
					printJob.print(textDocument, printerAttributes);
				}			
			} else {
				
				// Cria um job de impress�o
				DocPrintJob printJob = printer.createPrintJob();
				
				// Adiciona uma propriedade de impress�o: imprimir 2 c�pias
				printerAttributes.add(new Copies(2));
				
				// Imprime o documento sem exibir uma tela de dialogo
				printJob.print(textDocument, printerAttributes);
			}		
			
		} catch (FileNotFoundException ex) {
			System.out.println("Arquivo teste.txt n�o encontrado!");
		} catch (PrintException ex2) {
			System.out.println("Erro de impress�o: " + ex2.getMessage());
		}				
	}
	
	public static void main(String[] args) {
		new JpsPrintExample(true);
	}
}