import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;


/**
 * Exemplo de impress�o de um objeto Printable
 * utilizando lookup de impress�o fornecido
 * pela JPS
 * 
 * @author Herval Freire
 */
public class JpsPrintableExample {

	public JpsPrintableExample() {

		
		PrintService service = null;
		
		// JPS: Localiza todas as impressoras dispon�veis no sistema (inclusive impressoras em rede)
		PrintService[] printers = PrintServiceLookup.lookupPrintServices(null, null);

		// JPS: seleciona a �ltima impressora dispon�vel
		service = printers[printers.length-1];

		
		
		// Obtem um job de impressao
		PrinterJob job = PrinterJob.getPrinterJob();
		
		try {
			// Define que o printer job vai utilizar a impressora localizada pela JPS
			job.setPrintService(service);
		} catch (PrinterException e1) {
			System.out.println("Erro acessando impressora: " + e1.getMessage());
		}
		
		// Define o objeto a ser impresso
		job.setPrintable(new Desenho());
		
		
		// exibe o dialogo de impressao.
		if (job.printDialog()) {
			try {
				// imprime o objeto printable
				job.print();
			} catch (PrinterException e) {
				e.printStackTrace();
			}
		}	
		
	}
	
	public static void main(String[] args) {
		new PrintableExample();
	}
}