import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.standard.Chromaticity;

/**
 * @author Herval Freire
 */
public class LookupExample {
	public LookupExample() {
		// Localiza a impressora padr�o
		PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
		System.out.println("Impressora padr�o: " + printService.getName());
		
		// Localiza todas as impressoras dispon�veis no sistema (inclusive impressoras em rede)
		PrintService[] printers = PrintServiceLookup.lookupPrintServices(null, null);
		for (int i = 0; i < printers.length; i++) {
			System.out.println(printers[i].getName());
		}
		
		// Localiza todas as impressoras com suporte a impress�o de PDFs e com suporte a cores 
		DocFlavor pdfFlavor = DocFlavor.BYTE_ARRAY.PDF;
		
		AttributeSet attributes = new HashAttributeSet();
		attributes.add(Chromaticity.COLOR);

		PrintService[] pdfPrinters = PrintServiceLookup.lookupPrintServices(pdfFlavor, attributes);
		for (int i = 0; i < pdfPrinters.length; i++) {
			System.out.println(pdfPrinters[i].getName());
		}
	}
	
	public static void main(String[] args) {
		new LookupExample();
	}
}
