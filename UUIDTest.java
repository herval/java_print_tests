import java.sql.Timestamp;
import java.util.Date;

import org.doomdark.uuid.UUID;
import org.doomdark.uuid.UUIDGenerator;

/*
 * Created on 08/03/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author HFreire
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UUIDTest {
	public static void main(String[] args) {
		UUIDGenerator gen = UUIDGenerator.getInstance();
		UUID time = gen.generateTimeBasedUUID();
		System.out.println(time);

		time = gen.generateRandomBasedUUID();
		System.out.println(time);

		for (int i = 0; i < 10; i++) {			
			Timestamp tmp = new Timestamp(new Date().getTime());
			System.out.println(tmp.getTime() + " - " + System.currentTimeMillis());
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
