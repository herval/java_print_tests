import java.io.FileInputStream;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.SimpleDoc;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;

/*
 * Created on 08/03/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author HFreire
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PrintTest {
	public static void main(String[] args) {
		try {
			PrintService main = null;
			
			PrintService[] s = PrintServiceLookup.lookupPrintServices(null, null);
			for (int i = 0; i < s.length; i++) {
				System.out.println(s[i].getName());
				if (s[i].getName().startsWith("Zebra")) {
					main = s[i];
				}
				DocFlavor[] docs = s[i].getSupportedDocFlavors();
				for (int j = 0; j < docs.length; j++) {
					System.out.println(docs[j].getMimeType());
				}
				System.out.println("----");
			}
	
//			System.out.println("Printing...");
//			
//			Doc doc = new SimpleDoc(new FileInputStream("1000905276.jpg"), DocFlavor.INPUT_STREAM.JPEG, new HashDocAttributeSet());
//			
//			HashPrintRequestAttributeSet attr = new HashPrintRequestAttributeSet();
//			main = ServiceUI.printDialog(null, 200, 200, s, main, DocFlavor.INPUT_STREAM.JPEG, attr);
//			DocPrintJob job = main.createPrintJob();
//			
//			job.print(doc, attr);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
