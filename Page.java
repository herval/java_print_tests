import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

/*
 * Created on 08/03/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author HFreire
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Page extends PageFormat implements Printable {
	/* (non-Javadoc)
	 * @see java.awt.print.Printable#print(java.awt.Graphics, java.awt.print.PageFormat, int)
	 */
	public int print(Graphics g, PageFormat format, int arg2) throws PrinterException {
		/* Make a copy of the passed in Graphics instance so
		 * that we do not upset the caller's current Graphics
		 * settings such as the current color and font.
		 */
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setPaint(Color.black);

		float y = (float) (super.getImageableY() + super.getImageableHeight()-10);
//		 Cast to an int because of printing bug in drawString(String, float, float)!
		g2d.drawString("bla bla bla bla!!!", (int) super.getImageableX()+10, (int)y+10);
		g2d.drawString("bla bla bla bla!!!", (int) super.getImageableX()+30, (int)y+10);
		g2d.drawString("bla bla bla bla!!!", (int) super.getImageableX()+50, (int)y+10);
		g2d.drawString("bla bla bla bla!!!", (int) super.getImageableX(), (int)y);
		g2d.dispose();
		return Printable.PAGE_EXISTS;
	}

}
