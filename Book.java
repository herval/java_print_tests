import java.awt.print.PageFormat;
import java.awt.print.Pageable;
import java.awt.print.Printable;

/*
 * Created on 08/03/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author HFreire
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Book implements Pageable {

	/* (non-Javadoc)
	 * @see java.awt.print.Pageable#getNumberOfPages()
	 */
	public int getNumberOfPages() {
		return 1;
	}

	/* (non-Javadoc)
	 * @see java.awt.print.Pageable#getPageFormat(int)
	 */
	public PageFormat getPageFormat(int arg0) throws IndexOutOfBoundsException {
		return new PageFormat();
	}

	/* (non-Javadoc)
	 * @see java.awt.print.Pageable#getPrintable(int)
	 */
	public Printable getPrintable(int arg0) throws IndexOutOfBoundsException {
		return new Page();
	}

}
